import { DocumentChangeAction } from '@angular/fire/firestore';

export function mapSnapshotChanges(changes: DocumentChangeAction<any>[]) {
    return changes.map(c => {
        const data = c.payload.doc.data();
        return { key: c.payload.doc.id, ...data };
    })
}