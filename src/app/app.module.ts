import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { SharedModule } from './shared/shared.module';
import { RoutesModule } from './routes/routes.module';

import { ToastrModule } from 'ngx-toastr';
import { CookieModule } from 'ngx-cookie';
import { LaddaModule } from 'angular2-ladda';
import { NgxPaginationModule } from 'ngx-pagination';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { firebaseConfig } from './firebase.config';
import { firebaseConfigTwo } from './firebase.config';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {
  HttpClient,
  HttpClientModule
} from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpModule } from '@angular/http';
// https://github.com/ocombe/ng2-translate/issues/218

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpModule,
    BrowserAnimationsModule, // required for ng2-tag-input
    HttpClientModule,
    CoreModule,
    LayoutModule,
    SharedModule.forRoot(),
    CookieModule.forRoot(),
    RoutesModule,
    ToastrModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireModule.initializeApp(firebaseConfigTwo),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    LaddaModule.forRoot({
      style: "contract",
      spinnerSize: 30,
      spinnerColor: "red",
      spinnerLines: 12
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    NgxPaginationModule

  ],
  providers: [TranslateService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
