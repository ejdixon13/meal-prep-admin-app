import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HeadersService {

  constructor(private http: HttpClient) { }

  sendNotification(message) {
    const body = JSON.stringify(message);
    const headers = new HttpHeaders();
    headers.append('Authorization', 'Basic YjNmZDA0MGUtZGZhNy00YmVjLWE5ZjAtZDdkZTExN2E1NWVl');
    headers.append('Content-Type', 'application/json; charset=utf-8');
    return this.http.post('https://onesignal.com/api/v1/notifications', body, {
      headers: headers
    })

  }


}
