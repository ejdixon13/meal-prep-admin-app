import { Component } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
const swal = require('sweetalert');

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.scss']
})
export class CouponsComponent {
  public p = 1;
  coupons: Array<any>;
  couponsDataRef: AngularFireList<any>;
  couponObservable: Observable<any>;
  constructor(public af: AngularFireDatabase, public router: Router, public toastr: ToastrService) {
    this.couponsDataRef = af.list('/coupons');
    this.couponObservable = this.couponsDataRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      ));
    this.couponObservable.subscribe((res) => {
      this.coupons = res;
    });

  }


  couponDelete(key: any) {

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.couponsDataRef.remove(key).then((res) => {
          swal('Deleted!', 'Coupons Deleted Successfully!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }

}
