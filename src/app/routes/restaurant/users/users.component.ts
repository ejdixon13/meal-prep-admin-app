import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { mapSnapshotChanges } from 'src/app/shared/app.util';
const swal = require('sweetalert');

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {

  public p = 1;
  users: Array<any>;
  usersDataRef: AngularFirestoreCollection<any>;
  userObservable: Observable<any>;
  constructor(public afFS: AngularFirestore, public router: Router) {
    this.usersDataRef = afFS.collection('/users');
    this.userObservable = this.usersDataRef.snapshotChanges().pipe(
      map(changes =>
        mapSnapshotChanges(changes)
      ));
    this.userObservable.subscribe((res) => {
      this.users = res;
    })
  }

  usersShow(key) {
    this.router.navigate(['/users/viewUser', key]);
  }

  usersDelete(key: any) {

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.usersDataRef.doc(key).delete().then((res) => {
          swal('Deleted!', 'User Deleted Successfully!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }

}