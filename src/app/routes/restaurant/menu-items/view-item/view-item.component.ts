import { Component } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Component({
  selector: 'app-view-item',
  templateUrl: './view-item.component.html',
  styleUrls: ['./view-item.component.scss']
})
export class ViewItemComponent {

  menuDetails: any = {};
  menuItemsdataRef: AngularFirestoreDocument<any>;
  menuItemObservable: Observable<any>;
  categoryRef: AngularFirestoreDocument<any>;
  categoryObservable: Observable<any>;

  constructor(private route: ActivatedRoute, public router: Router, public afFS: AngularFirestore) {
    this.route.params.map(params => params['id']).subscribe((Id) => {
      if (Id != null) {
        this.menuItemsdataRef = this.afFS.doc('/menuItems/' + Id);
        this.menuItemObservable = this.menuItemsdataRef.valueChanges();
        this.menuItemObservable.subscribe((response) => {
          this.menuDetails = response;
          if (response) {
            this.categoryRef = this.afFS.doc('/categories/' + response.category);
            this.categoryObservable = this.categoryRef.valueChanges();
            this.categoryObservable.subscribe((res) => {

              if (res) {
                this.menuDetails.categoryTitle = res.title;
              }

            })
          }

        })
      }
    });
  }

}
