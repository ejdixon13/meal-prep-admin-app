import { Component } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { mapSnapshotChanges } from 'src/app/shared/app.util';
import * as moment from 'moment';
const swal = require('sweetalert');

@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.scss']
})
export class MenuItemsComponent {
  public p = 1; /// current page no.
  public totalMenuLength = 0;
  public currency: any = {};
  siteVal: any;
  menuItems: Array<any>;
  menuItemsDataRef: AngularFirestoreCollection<any>;
  menuObservable: Observable<any>;
  constructor(public afFS: AngularFirestore, public router: Router, public toastr: ToastrService) {
    if (localStorage.getItem('currency')) {
      this.currency = JSON.parse(localStorage.getItem('currency'));
    }
    this.menuItemsDataRef = this.afFS.collection('/menuItems');
    this.menuObservable = this.menuItemsDataRef
      .snapshotChanges()
      .pipe( map(changes => mapSnapshotChanges(changes)) );
    this.menuObservable.subscribe((res) => {
      this.menuItems = res;
      console.log(res);
      if (this.menuItems.length > 0) {

        this.totalMenuLength = this.menuItems.length;
      }
    });
  }


  getMenuItems(ev: any) {
    let val = ev;

    this.menuObservable = this.afFS.collection('/menuItems', ref => ref.orderBy('title').startAt(val.charAt(0).toUpperCase() + val.slice(1))
      .endAt(val.charAt(0).toUpperCase() + val.slice(1) + '\uf8ff'))
      .snapshotChanges()
      .map(changes => mapSnapshotChanges(changes) );
    this.menuObservable.subscribe((res) => {
      this.menuItems = res;
    });


  }

  menuItemShow(key) {
    this.router.navigate(['/menu/viewItems', key]);
  }

  menuItemEdit(key) {
    this.router.navigate(['/menu/editItems', key]);
  }

  menuItemDelete(key: any) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.menuItemsDataRef.doc(key).delete().then((res) => {
          swal('Deleted!', 'Menu Item Deleted Successfully!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }

  isItemCurrentlyShown(item): boolean {
    if (item.startDate && item.endDate) {
      const today = moment().format('YYYY-MM-DD');
      return item.startDate <= today && item.endDate >= today;
    } else {
      return false;
    }
  }

}
