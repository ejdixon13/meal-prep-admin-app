import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';;

@Component({
  selector: 'app-subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.scss']
})
export class SubscribersComponent implements OnInit {
  public p = 1;//Starting page
  public subsCribeLength = 0;
  subscribersData: Array<any>;
  subscribeDataRef: AngularFireList<any>;
  subscribeObservable: Observable<any>;

  constructor(public af: AngularFireDatabase) {
    this.subscribeDataRef = af.list('/subscribe');
    this.subscribeObservable = this.subscribeDataRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      ));

    this.subscribeObservable.subscribe((res) => {
      this.subscribersData = res;
      if (this.subscribersData.length > 0) {
        this.subsCribeLength = this.subscribersData.length;
      }
    });
  }

  // getnews(ev: any) {
  //     let val = ev;
  //     this.subscribeObservable = this.af.list('/subscribe', ref => ref.orderByChild('email').startAt(val.charAt(0).toUpperCase() + val.slice(1))
  //        .endAt(val.charAt(0).toUpperCase() + val.slice(1) + '\uf8ff')).valueChanges();
  //     this.subscribeObservable.subscribe((data) => {
  //             this.subscribersData = data;
  //         });
  // }

  ngOnInit() {
  }

}
