import { Component } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
const swal = require('sweetalert');

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent {
  public p = 1; // start page no.
  public newsLength = 0;
  siteVal: any;
  news: Array<any>;
  newsDataRef: AngularFireList<any>;
  newsObservable: Observable<any>;

  constructor(public af: AngularFireDatabase, public router: Router, public toastr: ToastrService) {
    this.newsDataRef = af.list('/news');
    this.newsObservable = this.newsDataRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      ));

    this.newsObservable.subscribe((res) => {
      this.news = res;
      if (this.news.length > 0) {
        this.newsLength = this.news.length;
      }
    });
  }


  getnews(ev: any) {
    let val = ev;
    this.newsObservable = this.af.list('/news', ref => ref.orderByChild('title').startAt(val.charAt(0).toUpperCase() + val.slice(1))
      .endAt(val.charAt(0).toUpperCase() + val.slice(1) + '\uf8ff')).snapshotChanges().map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });
    this.newsObservable.subscribe((data) => {
      this.news = data;
    });


  }


  newsShow(key) {
    this.router.navigate(['/news/viewNews', key]);
  }

  newsEdit(key) {
    this.router.navigate(['/news/editNews', key]);
  }

  menuItemEdit(key) {
    this.router.navigate(['/menu/editItems', key]);
  }

  newsDelete(key: any) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.newsDataRef.remove(key).then((res) => {
          swal('Deleted!', 'News Deleted Successfully!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }
}
