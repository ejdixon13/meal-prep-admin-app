import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { mapSnapshotChanges } from 'src/app/shared/app.util';
import { some } from 'lodash';
const swal = require('sweetalert');

@Component({
  selector: 'app-delivery-options',
  templateUrl: './delivery-options.component.html',
  styleUrls: ['./delivery-options.component.scss']
})
export class DeliveryOptionsComponent implements OnInit {
  public p = 1;
  public allPincodes: any[] = [];
  public activeAddBlock: boolean = false;
  public activeUpdateBlock: boolean = false;
  public activeDataTable: boolean = true;
  private updateObjKey: any;
  public deliveryDays = [
    {
      checked: false,
      label: 'Sunday'
    },
    {
      checked: false,
      label: 'Monday'
    },
    {
      checked: false,
      label: 'Tuesday'
    },
    {
      checked: false,
      label: 'Wednesday'
    },
    {
      checked: false,
      label: 'Thursday'
    },
    {
      checked: false,
      label: 'Friday'
    },
    {
      checked: false,
      label: 'Saturday'
    }
  ]

  add: any = {
    pincode: '',
    zipCodeList: []
  }
  pinDataRef: AngularFirestoreCollection<any>;
  pinObservable: Observable<any>;
  pinObjRef: AngularFirestoreDocument<any>;

  update: any = {
    pincode: '',
    zipCodeList: []
  }

  constructor(public afFS: AngularFirestore, public toastr: ToastrService, ) {
    this.pinDataRef = this.afFS.collection('/delivery-options');
    this.pinObservable = this.pinDataRef.snapshotChanges().pipe(
      map(changes => mapSnapshotChanges(changes))
    );
    this.pinObservable.subscribe((res) => {
      this.allPincodes = res;
    });
  }

  activeAdd() {
    this.activeAddBlock = true;
    this.activeUpdateBlock = false;
    this.activeDataTable = false;
  }

  onAddPin() {
    this.pinDataRef.doc(this.add.city).set({
      zipCodeList: this.add.pincode,
      deliveryDays: this.deliveryDays.filter(day => day.checked).map(day => day.label)
    }).then((res) => {
      this.toastr.success('Delivery Area Added!', 'Success!');
      this.cancelAdd();
    })
  }

  isAllRequiredAddFieldsFilledOut() {
    if (this.add.pincode && this.add.city) {
      return true;
    }
    return false;
  }

  isAllRequiredUpdateFieldsFilledOut() {
    if (this.update.pincode && this.update.city) {
      return true;
    }
    return false;
  }

  activeUpdate(key, i) {
    this.activeUpdateBlock = true;
    this.activeAddBlock = false;
    this.activeDataTable = false;
    const selectedItem = this.allPincodes[i];
    this.update = {
      city: selectedItem.key,
      pincode: selectedItem.zipCodeList.join(', ')
    };
    this.deliveryDays = this.deliveryDays.map(day => {
      return {
        checked: some(selectedItem.deliveryDays, d => d === day.label),
        label: day.label
      }
    });
    this.updateObjKey = key;
  }

  onUpdatePin() {
    this.pinObjRef = this.afFS.doc('/delivery-options/' + this.updateObjKey);
    this.pinObjRef.update({
      zipCodeList: this.update.pincode.split(',').map(zip => zip.trim()),
      deliveryDays: this.deliveryDays.filter(day => day.checked).map(day => day.label)
    }).then((res) => {
      this.toastr.success('Delivery Area Updated!', 'Success!');
      this.cancelUpdate();
    })
  }

  cancelAdd() {
    this.activeDataTable = true;
    this.activeUpdateBlock = false;
    this.activeAddBlock = false;
    this.add.pincode = 0;
  }

  cancelUpdate() {
    this.updateObjKey = '',
      this.activeDataTable = true;
    this.activeUpdateBlock = false;
    this.activeAddBlock = false;
  }

  pinDelete(key) {

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.pinDataRef.doc(key).delete().then((res) => {
          swal('Deleted!', 'Delivery Area Deleted!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }

  ngOnInit() {
  }

}
