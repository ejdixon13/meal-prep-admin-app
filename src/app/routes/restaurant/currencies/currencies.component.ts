import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
const swal = require('sweetalert');


@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.scss'],
  providers: []
})

export class CurrenciesComponent implements OnInit {

  public currency: any[] = [];
  public loading: boolean = false;
  private currencytDataRef: AngularFireList<any>;
  private currencyObservable: Observable<any>;

  constructor(public router: Router,
    public af: AngularFireDatabase,
    public toastr: ToastrService) {
    this.getCurrencyData();

  }

  ngOnInit() {
  }



  getCurrencyData() {
    this.currencytDataRef = this.af.list('/currency');
    this.currencyObservable = this.currencytDataRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      ));
    this.currencyObservable.subscribe((res: any) => {
      if (res.length > 0) {
        this.currency = res;
      } else {
        this.currency = [];
      }
    });
  }

  currencyEdit(key) {
    this.router.navigate(['/currencies/editCurrency', key]);
  }

  currencyDelete(key: any, i: any) {

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.currencytDataRef.remove(key).then((res) => {
          swal('Deleted!', 'currency Deleted Successfully!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }


}
