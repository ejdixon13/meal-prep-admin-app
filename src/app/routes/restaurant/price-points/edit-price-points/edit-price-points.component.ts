import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { ToastrService } from 'ngx-toastr';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-edit-price-points',
  templateUrl: './edit-price-points.component.html',
  styleUrls: ['./edit-price-points.component.scss'],
  providers: []
})
export class EditPricePointsComponent implements OnInit {
  id: string;
  mealPlan: any = {
    pricePerMeal: 0,
    numMealsPerWeek: 0,
    color: ''
  };
  public isLoading: boolean = false;
  private currencyRef: AngularFirestoreDocument<any>;
  constructor(private toster: ToastrService, private route: ActivatedRoute, private router: Router, public afFS: AngularFirestore) {
    this.route.params.map((params) => params['id']).subscribe((id) => {
      this.id = id;
      if (this.id != null) {
        this.getMealPlan(this.id);
      }
    });
  }

  ngOnInit() {
  }

  // get selected currency data
  getMealPlan(Id) {
    this.afFS.doc('/mealPlans/' + Id).valueChanges()
      .subscribe((response) => {
        this.mealPlan = response;
      })
  }

  // update selected currency data
  onUpdateMealPlan(NgForm) {
    this.afFS.doc('/mealPlans/' + this.id).update(this.mealPlan).then(() => {
      this.toster.success('Meal Plan Updated Successfully!', 'Success!');
      this.router.navigate(['/pricePoints/managePricePoints']);
    }, error => {
      this.toster.error('Meal Plans Not Updated !', 'Error!');
    })
  }

  cancel() {
    this.router.navigate(['/pricePoints/managePricePoints']);
  }

}
