import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { mapSnapshotChanges } from 'src/app/shared/app.util';
const swal = require('sweetalert');


@Component({
  selector: 'app-price-points',
  templateUrl: './price-points.component.html',
  styleUrls: ['./price-points.component.scss'],
  providers: []
})

export class PricePointsComponent implements OnInit {

  public mealPlans: any[] = [];
  public loading: boolean = false;
  private mealPlansRef: AngularFirestoreCollection<any>;
  private mealPlansObservable: Observable<any>;

  constructor(public router: Router,
    public afFS: AngularFirestore,
    public toastr: ToastrService) {
    this.getMealPlansData();

  }

  ngOnInit() {
  }



  getMealPlansData() {
    this.mealPlansRef = this.afFS.collection('/mealPlans');
    this.mealPlansObservable = this.mealPlansRef.snapshotChanges().pipe(
        map(changes => mapSnapshotChanges(changes))
    );
    this.mealPlansObservable.subscribe((res: any) => {
      if (res.length > 0) {
        this.mealPlans = res;
      } else {
        this.mealPlans = [];
      }
    });
  }

  mealPlanEdit(key) {
    this.router.navigate(['/pricePoints/editPricePoints', key]);
  }

  mealPlanDelete(key: any, i: any) {

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.mealPlansRef.doc(key).delete().then((res) => {
          swal('Deleted!', 'Meal Plan Deleted Successfully!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }


}
