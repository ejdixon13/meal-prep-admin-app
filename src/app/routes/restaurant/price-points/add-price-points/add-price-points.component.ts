import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-add-price-point',
  templateUrl: './add-price-points.component.html',
  styleUrls: ['./add-price-points.component.scss'],
  providers: []
})
export class AddPricePointsComponent implements OnInit {

  mealPlan: any = {
    pricePerMeal: 0,
    numMealsPerWeek: 0,
    color: ''
  };
  isLoading: boolean = false;

  constructor(private toastr: ToastrService, private route: Router, private afFS: AngularFirestore) {
  }

  ngOnInit() {
  }

  onAddPricePoint() {
    this.afFS.collection('mealPlans').doc(this.mealPlan.numMealsPerWeek + 'x').set(this.mealPlan).then(() => {
      this.toastr.success('Price Point Added Successfully!', 'Success!');
      this.route.navigate(['/pricePoints/managePricePoints']);
    })
  }

  cancel() {
    this.route.navigate(['/pricePoints/managePricePoints']);
  }

}
