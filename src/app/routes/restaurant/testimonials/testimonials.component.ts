import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
const swal = require('sweetalert');

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {
  public p = 1; //staring pagination
  public testimonialLength = 0;
  public siteVal: any;
  public testimonials: any[] = [];
  public testimonialRef: AngularFireList<any>;
  public testimonialsData: Observable<any>;

  constructor(public af: AngularFireDatabase,
    public router: Router,
    public toastr: ToastrService) {
    this.testimonialRef = this.af.list('/testimonials');
    this.testimonialsData = this.testimonialRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
    this.testimonialsData.subscribe((res) => {
      this.testimonials = res;
      if (this.testimonials.length > 0) {
        this.testimonialLength = this.testimonials.length;
      }
    });
  }

  searchTestimonials(ev: any) {
    let val = ev;
    this.testimonialsData = this.af.list('/testimonials', ref => ref.orderByChild('name').startAt(val.charAt(0).toUpperCase() + val.slice(1))
      .endAt(val.charAt(0).toUpperCase() + val.slice(1) + '\uf8ff')).snapshotChanges().map(changes => {
        return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
      });
    this.testimonialsData
      .subscribe((data) => {
        this.testimonials = data;
      });


  }
  viewTestimonial(key) {
    this.router.navigate(['/testimonials/viewTestimonials', key]);
  }

  updateTestimonial(key) {
    this.router.navigate(['/testimonials/editTestimonials', key]);
  }

  testimonialDelete(key: any) {
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.testimonialRef.remove(key).then(resp => {
          swal('Deleted!', 'Testimonial Data Deleted Successfully!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }
  ngOnInit() {
  }



}
