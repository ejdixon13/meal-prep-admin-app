import { Component } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from "@angular/router";
import { OrdersService } from './orders.service';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { orderBy } from 'lodash';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  providers: [OrdersService]
})
export class OrdersComponent {
  public p = 1; // start page;
  public ordersLength = 0;
  public currency: any = {};
  orders: Array<any>;
  ordersDataRef: AngularFirestoreCollection<any>;
  orderObservable: Observable<any>;
  updatedOnce: boolean = false;

  userObjRef: AngularFirestoreDocument<any>;
  orderObjRef: AngularFirestoreDocument<any>;
  loyalityData: AngularFireObject<any>;
  loylityPercentage: number = 0;

  public loyalityStatus: boolean = false;
  public minLoyalityPointes: number = 0;

  private orderData: any = {};
  private userData: any = {};
  private playerId: string;

  constructor(
    private afFS: AngularFirestore,
    public toastr: ToastrService,
    public router: Router,
    public ordersService: OrdersService) {

    if (localStorage.getItem('currency')) {
      this.currency = JSON.parse(localStorage.getItem('currency'));
    }
    this.ordersDataRef = this.afFS.collection('/orders');
    this.orderObservable = this.ordersDataRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => {
          const data = c.payload.doc.data() as object;
          return { key: c.payload.doc.id, ...data };
        })
      )
    );
    this.orderObservable.subscribe((res) => {
      console.log(res);
      this.orders = res.reverse();
      this.orders = orderBy(this.orders, ['deliveryDate'], ['desc']);
      if (this.orders.length > 0) {
        this.ordersLength = this.orders.length;
      }
    });

    // loality points

    // this.loyalityData = af.object('/loyalitys');
    // this.loyalityData.valueChanges().subscribe((res: any) => {
    //   //console.log("loyalityData res "+JSON.stringify(res));
    //   if (res != null) {
    //     this.loyalityStatus = res.enable;
    //     this.loylityPercentage = res.loylityPercentage;
    //   }
    // });

  }

  OnChangeStatus(key, event, userId) {

    this.orderObjRef = this.afFS.doc("/orders/" + key);
    this.userObjRef = this.afFS.doc("/users/" + userId);


    if (event.target.value === 'Delivered' && this.loyalityStatus) {

      let data = this.orderObjRef.valueChanges().subscribe((res) => {
        this.orderData = res;

        let userPoint: number;
        userPoint = Math.floor((this.orderData.grandTotal * this.loylityPercentage) / 100);

        data.unsubscribe();

        let loayltyObj: any = {
          credit: true,
          points: userPoint,
          orderId: key,
          createdAt: Date.now()
        }
        // this.af.list('users/' + userId + '/loyaltyPoints').push(loayltyObj);
        // this.af.list('/orders/' + key + '/loyaltyPoints').push(loayltyObj);
        // })

        //this.userInfo.loyaltyPoints.push(Math.floor(addPoint));
        //console.log("added point " + JSON.stringify(this.orderData));

        // this.updateLoalityStatus(event, key, userId);
        //        if(this.orderData.loyaltyPoints == null){
        //          let loylityPoints:any [] = [];
        //          //console.log("lo loyaltyPoints");
        //          this.orderData.loyaltyPoints = loylityPoints;
        //          //console.log("now order "+JSON.stringify(this.orderData));
        //        }

        //        let addPoint:number;
        //        addPoint = Math.floor((this.orderData.grandTotal * this.loylityPercentage)/100);
        //        this.orderData.loyaltyPoints.push({
        //          credit:true,
        //          points: addPoint,
        //          orderId: key,
        //          createdAt:Date.now()
      });

      //    let user =   this.userObjRef.valueChanges().subscribe((res) => {
      //      this.userData = res;
      //      //console.log("userData is before"+JSON.stringify(this.userData));
      // if(this.userData.loyaltyPoints == null){
      //   let loylityPoints:any [] = [];
      //   //console.log("lo loyaltyPoints");
      //   this.userData.loyaltyPoints = loylityPoints;
      //   //console.log("now userData "+JSON.stringify(this.userData));
      // }
      //user.unsubscribe();




      //});



    }// if outer closed

    else {
      this.updateStatus(key, event, userId);
    }
  }

  // updateLoalityStatus(event, key, userId) {
  //   //if(this.updatedOnce == false){
  //   this.orderObjRef.update({
  //     status: event.target.value,
  //     orderView: true
  //   }).then((res: any) => {

  //     this.af.list('/orders/' + key + '/statusReading').push({ title: event.target.value, time: Date.now() }).then(() => {
  //       this.af.object('users/' + userId + '/' + 'playerId').valueChanges().subscribe((respo: any) => {
  //         // console.log(respo);
  //         if (respo) {
  //           var message = {
  //             app_id: "9740a50f-587f-4853-821f-58252d998399",
  //             contents: { "en": "Your Loyality Points Creadited" },
  //             include_player_ids: [respo]
  //           };
  //           this.ordersService.sendNotification(message).subscribe(response => {

  //           });
  //         }
  //         this.toastr.success('Order status updated!', 'Success!');

  //       });

  //     });


  //   });
  //   //this.updatedOnce = true;
  //   // }
  // }

  updateStatus(key, event, userId) {

    this.ordersDataRef.doc(key).update({ status: event.target.value, orderView: true }).then((res) => {

      this.afFS.doc('/orders/' + key + '/statusReading').update({ title: event.target.value, time: Date.now() }).then(() => {
        this.afFS.doc('users/' + userId + '/' + 'playerId').valueChanges().subscribe((respo: any) => {
          // console.log(respo);
          if (respo) {
            var message = {
              app_id: "9740a50f-587f-4853-821f-58252d998399",
              contents: { "en": "Your Order Status: " + event.target.value },
              include_player_ids: [respo]
            };
            this.ordersService.sendNotification(message).subscribe(response => {

            });
          }
          this.toastr.success('Order status updated!', 'Success!');
        });
      });


    });
    //}
  }


  ordersShow(key) {
    this.ordersDataRef.doc(key).update({ orderView: true }).then((res) => {
      this.router.navigate(['/order/viewOrder', key]);
    });
  }

}