import { Component, OnInit } from '@angular/core';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { mapSnapshotChanges } from 'src/app/shared/app.util';
const swal = require('sweetalert');


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  public p = 1; //start page no. 
  public categoryLength = 0;
  public siteVal: any;
  public categories: any[] = [];
  public catRef: AngularFirestoreCollection<any>;
  public categoryData: Observable<any>;

  constructor(public afFS: AngularFirestore,
    public router: Router,
    public toastr: ToastrService) {

    this.catRef = this.afFS.collection('/categories');
    this.categoryData = this.catRef.snapshotChanges()
      .pipe(
        map(changes => mapSnapshotChanges(changes))
      );
    this.categoryData.subscribe((res) => {
      this.categories = res;
      console.log(res);
      if (this.categories.length > 0) {
        this.categoryLength = this.categories.length;
      }
    });
  }



  getCategory(ev: any) {
    let val = ev;
    // console.log(val, 'ankur');
    this.categoryData = this.afFS.collection('/categories', ref => ref.orderBy('title').startAt(val.charAt(0).toUpperCase() + val.slice(1))
      .endAt(val.charAt(0).toUpperCase() + val.slice(1) + '\uf8ff'))
      .snapshotChanges().map(changes => mapSnapshotChanges(changes));
    this.categoryData.subscribe((res) => {
      console.log(res);
      this.categories = res;
    });


  }


  categoryShow(key) {
    this.router.navigate(['/categories/viewCategory', key]);
  }

  categoryEdit(key) {
    this.router.navigate(['/categories/editCategory', key]);
    console.log('ankur')
  }

  categoryDelete(key: any) {

    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        this.catRef.doc(key).delete().then(resp => {
          swal('Deleted!', 'Categories Data Deleted Successfully!', { icon: "success" });
        })
      } else {
        swal('Cancelled', 'Your data is safe :)');
      }
    });
  }

  ngOnInit() {

  }
}
