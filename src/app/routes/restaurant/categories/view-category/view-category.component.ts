import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { NgForm } from '@angular/forms';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.scss']
})
export class ViewCategoryComponent implements OnInit {

  public categoryDetails: any = {};
  public catRef: AngularFirestoreDocument<any>;
  public catObservable: Observable<any>;
  constructor(private route: ActivatedRoute,
    public router: Router,
    public afFS: AngularFirestore) {
    this.route.params.map(params => params['id']).subscribe((Id) => {
      if (Id != null) {
        this.catRef = this.afFS.doc('/categories/' + Id);
        this.catObservable = this.catRef.valueChanges();
        this.catObservable.subscribe((response) => {
          this.categoryDetails = response;
        });
      }
    });
  }

  ngOnInit() {

  }

}