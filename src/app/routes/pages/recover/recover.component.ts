import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
    selector: 'app-recover',
    templateUrl: './recover.component.html',
    styleUrls: ['./recover.component.scss']
})
export class RecoverComponent implements OnInit {

    valForm: FormGroup;
    public af: AngularFireAuth;


    constructor(public settings: SettingsService, fb: FormBuilder) {
        this.valForm = fb.group({
            'email': [null, Validators.compose([Validators.required, CustomValidators.email])]
        });

    }

    // submitForm($ev, value: any, email: string) {
    //     $ev.preventDefault();
    //     for (let c in this.valForm.controls) {
    //         this.valForm.controls[c].markAsTouched();
    //     }
    //     console.log(value);
    //     this.af.auth.sendPasswordResetEmail(value).then(() => console.log("email sent"))
    //         .catch((error) => console.log(error))
    // }


    ngOnInit() {
    }

}
